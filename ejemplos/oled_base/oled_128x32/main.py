from machine import Pin, SoftI2C
import ssd1306

i2c = SoftI2C(scl=Pin(22), sda=Pin(21))
w, h = 128, 32

oled = ssd1306.SSD1306_I2C(w, h, i2c)
oled.text('PyCamp2021',0,0)
oled.show()

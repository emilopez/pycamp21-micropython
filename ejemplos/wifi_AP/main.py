import network

ap = network.WLAN(network.AP_IF) # create access-point interface
ap.config(essid='uPy3456789', authmode=network.AUTH_WPA_WPA2_PSK, password="uPy12345", max_clients=2) # the length of the essid must be of 10 chars and password length of 8 chars
ap.active(True)         # activ

import utime
from machine import Pin

pir = Pin(23, Pin.IN)
led = Pin(2, Pin.OUT)

while True:
    if pir.value() != 0:
        led.on()
    else:
        led.off()
    
    utime.sleep(0.1)
from utime import sleep
from machine import Pin, PWM

movimiento = False

def handle_interrupt(pin):
    global movimiento
    movimiento = True

led = Pin(2, Pin.OUT)
pir = Pin(23, Pin.IN)

pir.irq(trigger=Pin.IRQ_RISING, handler=handle_interrupt)

while True:
    if movimiento:
        #print("Movimiento!") OJO CON INTERRUPCIONES SE CUELGA SI HAY PRINT
        led.on()
        sleep(2)
        led.off()
        #print("SIN movimiento!")
        movimiento = False


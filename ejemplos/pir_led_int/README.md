Hay que indicarle cuándo disparar la interrupción, en este caso en RISING y la función que se va a disparar (handler) cuando eso suceda. 
**OJO, los handlers deben hacer algo que no tarde mucho y evitar el uso de print pq se cuelga.**
Lo recomendable es cambiar el estado de alguna variable y luego hacer lo que quieras según este valor fuera del handler.

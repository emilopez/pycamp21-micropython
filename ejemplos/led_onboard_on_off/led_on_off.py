import utime
from machine import Pin

led = Pin(2, Pin.OUT)

while True:
    led.value(not led.value())
    utime.sleep(0.5)
import network
import uping
from led_board import blink_led

my_wifi = 'wifi_ssid'
my_wifi_pass = 'wifi_pass'


def do_connect():
    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)
    if not wlan.isconnected():
        # print('connecting to network...')
        wlan.connect(my_wifi, my_wifi_pass)
        while not wlan.isconnected():
            blink_led(2, 0.10)
            pass
    ifconfig = wlan.ifconfig()
    info = {
        'ip': ifconfig[0],
        'netmask': ifconfig[1],
        'gateway': ifconfig[2],
        'dns': ifconfig[3]}
    return info

def check_internet():
    ping = uping.ping('8.8.8.8')
    percent_ping = int((ping[1] / ping[0])*100)
    return percent_ping



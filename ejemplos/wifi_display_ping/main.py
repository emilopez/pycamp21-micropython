from wifi import do_connect, check_internet
from machine import Pin, SoftI2C
from led_board import blink_led
import ssd1306
from time import sleep
import progress_bar

info = do_connect()
blink_led(1, 0.25)
qos = 50

i2c = SoftI2C(scl=Pin(22), sda=Pin(21))
oled_width = 128
oled_height = 32
oled = ssd1306.SSD1306_I2C(oled_width, oled_height, i2c)
my_bar = progress_bar.ProgressBar(0, 21, oled_width - 10, 12, oled)

while True:
    internet = check_internet()
    if internet==0:
        qos-=5
    else:
        qos+=1
    if qos > 100:
        qos = 100
    elif qos<1:
        qos = 1
    oled.init_display()
    oled.text('PyCamp 2021',0,0)
    oled.text('ip:' + info['ip'],0,10)
    my_bar.set_text(str(qos), 0)
    my_bar.update()
    oled.show()
    sleep(3)

from machine import Pin
from time import sleep

led = Pin(2,Pin.OUT)

def blink_led(cantidad, velocidad):
    for i in range(cantidad):
        led.value(1)
        sleep(velocidad)
        led.value(0)
        sleep(velocidad)

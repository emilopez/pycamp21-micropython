## Pycamp 2021
### Micropython en ESP32

Se instaló upython en la placa y se utilizaron las librerías para conectarse al wifi del pycamp. Tambien se conecto un pequeño display oled y en primera instancia la forma de unir ambas caracteristicas fue mostrar la ip que le daba el dhcp a nuestra placa.

En segunda instancia instalamos una librería para poder hacer ping desde la placa. Y una forma de dibujar una barra de progreso en el display.

Con estas herramientas, aplicamos una lógica simple de medir que calidad de ping hacia internet, en el cual del del 1 al 100 se restan 5 si el ping no responde y se suma 1 si el ping responde, cada 3 segundos. Esto nos da una idea temporal de la desconexion de nuestro router hacia internet (no tiene en cuenta el ancho de banda en el trancurso del tiempo o calidad de servicio)

### Pasos de instalación y uso

1. Descargar la carpeta completa en nuestra pc.
2. Configurar el ssid y pass de nuestra red wifi en el archivo wifi.py
3. Subir todos los archivos a la placa por usb, por ej con thonny.
4. Guardar y resetear la placa.

<img src="img/hola_pycamp.png" width=500>

### Fuentes
* [Libreria para utilizar el display](https://randomnerdtutorials.com/micropython-oled-display-esp32-esp8266/)

* [Libreria para dibujar una barra de progreso en el display](https://github.com/follower46/micropython-oled-progressbars)

* [Libreria uping para testear](https://gist.github.com/shawwwn/91cc8979e33e82af6d99ec34c38195fb)



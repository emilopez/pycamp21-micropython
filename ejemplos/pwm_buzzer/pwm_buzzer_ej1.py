import utime
from machine import Pin, PWM

freq = 5000

buzz = PWM(Pin(5), freq) 


while True:
    for dc in range(1024):
        buzz.duty(dc)
        utime.sleep(0.005)

"""
Escribimos datos en la flash, OJO con pisar los scripts, por eso lo hago dentro del
directorio datos
"""
import os
import random

os.chdir('/')

for f in os.listdir():
    print(f)

# creamos directorio datos si no existe
if 'datos' not in os.listdir():
    os.mkdir('datos')

# nos cambiamos a ese dir
os.chdir('datos')

# creamos un archivo y escribimos datos
fout = open('test.csv', 'a')
fout.write(str(random.random()))
fout.write(';')
fout.write(str(random.random()))
fout.write('\n')
fout.close()

# mostramos lo escrito
for linea in open('test.csv'):
    print(linea.strip())
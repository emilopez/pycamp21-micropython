import sh1106
from machine import Pin, SoftI2C
i2c = SoftI2C(scl=Pin(22), sda=Pin(21))
display = sh1106.SH1106_I2C(128, 64, i2c, Pin(16), 0x3c)

display.text('Testing 1', 0, 0, 1)
display.show()
display.sleep(True) # la apaga, consume 0.01mA

display.sleep(False) # lo enciende
import esp32
import utime
from machine import Pin

led = Pin(2, Pin.OUT)

while True:
    
    print((esp32.raw_temperature()-32)/1.8) # read the internal temperature of the MCU
    led.value(not led.value())
    utime.sleep(0.5)
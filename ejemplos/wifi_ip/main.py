from machine import Pin, SoftI2C

import utime
import network
import ssd1306

def listar_redes():
    for (ssid, _,_,_,_,_) in wlan.scan():
        print(ssid.decode("utf-8"))
    
def mostrar_ip_en_oled():
    i2c = SoftI2C(scl=Pin(22), sda=Pin(21))
    oled = ssd1306.SSD1306_I2C(128,64,i2c)
    oled.text(wlan.ifconfig()[0],0,0)
    oled.show()
    

wlan = network.WLAN(network.STA_IF)
wlan.active(True)
wlan.connect('5D', 'NOLACOMPARTO')
utime.sleep(2)
print(wlan.ifconfig())
mostrar_ip_en_oled()


# Pycamp 2021 Micropython en ESP32 / ESP8266

Aprendiendo y documentdanto micropython en el Pycamp 2021,  Los Reyunos, Mendoza

<img src="img/fin.jpg" width=400>

## Info útil de la placa

- Nombre: NodeMCU-32S
- [Placa en MLibre](https://articulo.mercadolibre.com.ar/MLA-916790826-nodemcu-esp32-wifi-bluetooth-42-iot-wroom-esp32s-arduino-_JM#position=1&search_layout=grid&type=item&tracking_id=4c6ca0b0-2148-414c-b62c-f8a3cbc03251)
- Pinout

<img src="img/pinout_nodemcu_esp32-full.jpg" width=500>

- Características
    - 448 KByte ROM
    - 520 KByte SRAM
    - 16 KByte SRAM in RTC
    - Flash/SRAM, 4 MBytes
    - 18 canales ADC
    - 3 interfaces SPI
    - 3 UART
    - 2 interfaces I2C
    - Salidas 16 PWM
    - Dos DAC de 8 bits
    - 10 entradas táctiles capacitivas
    - SRAM interno de 520 kB
    - Corriente de Deep Sleep de 2.5 µA
    - 28 GPIO
    - Wi-Fi de 802.11 b/g/n
    - Bluetooth integrado de modo dual (clásico y BLE)
    - Rango operativo de 2.2 V a 3.6 V (5v por micro-USB)
    - Soporte de 10 electrodos táctiles capacitivos
  
- **Referencia a la ESP32:** https://docs.micropython.org/en/latest/esp32/quickref.html  
- **Documentación oficial micropython:** https://docs.micropython.org/en/latest/reference/index.html 
- **Ejemplos de la doc oficial:** https://github.com/micropython/micropython/tree/master/examples
    

## Flashear el firmware Micropython a la placa
- Descargar el bin de micropython de https://micropython.org/download/esp32/, por ej [esta](https://micropython.org/resources/firmware/esp32-20210902-v1.17.bin)
- Instalar: `pip install esptool`
- Confirmar que estamos en grupo "dialout" (en /etc/group)
- Conectar la placa y ver su nombre (`dmesg`)
- Borrar la flash:
`esptool.py --chip esp32 --port /dev/ttyUSB0 erase_flash`
- Flasheamos micropython:
`esptool.py --chip esp32 --port /dev/ttyUSB0 --baud 460800 write_flash -z 0x1000 esp32-20210902-v1.17.bin`

## Enviarle tu programa python a la placa
Para que tu programa se ejecute cada vez que se enciende la placa debe llamarse `main.py` (ojo, **no puede** llamarse `boot.py`)

### Por consola
Podemos usar `ampy` o `mpremote`. Los instalamos:  `pip install adafruit-ampy mpremote`

- Pasar programa `main.py`

`ampy --port /dev/ttyUSB0 --baud 115200 put src/esp/pir_interr_ej1/main.py`

- Ejecutar un programa que le pasamos (no lo graba), **muy util para probar**

`mpremote run src/esp/ble_temperature_central/main.py`

- Conectar en modo REPL

    - `mpremote connect /dev/ttyUSB0 repl`

- Queremos ver los archivos que tiene

    - `mpremote ls`

- Transfiero archivos desde/hacia la placa

    - `mpremote cp <src> <dst>`

Cuando src o dst sea la placa hay que anteponer unos dos puntos `:`, por ejemplo me traigo boot.py de la placa: `mpremote cp :boot.py .`


### Con IDE

- Con **Thonny** 
    - instalarlo mediante pip: `pip install thonny`
    - configurar la placa que tenemos en tools/options/interpreter, en nuestro caso MicroPython (ESP32)
    - Programamos, y oara pasarlo: save as, microptyhon device
    - En la parte inferior tenemos el modo REPL  

## Ejemplos

En ejemplos/ se encuentran los siguientes códigos:

- `boot.py`: script por defecto, **no debe sobreescribirse!!**
- `internal_temp_esp32`: lee temperatura del chip
- `led_onboard_on_off`: blink led onboard
- `pir_base_no_int`: Leer sensor de movimiento PIR y prender led (sin interrupciones) ([sensor](https://articulo.mercadolibre.com.ar/MLA-736251823-sensor-pir-sr501-movimiento-infrarrojo-arduino-ubot-_JM#position=2&search_layout=stack&type=item&tracking_id=a4786aca-dd95-4c33-bbe2-1e03eb5f77e2))
- `pir_led_int`: Leer sensor de movimiento PIR y prender led (**con interrupciones**)
- `pwm_buzzer`: PWM (Buzzer) ([sensor](https://articulo.mercadolibre.com.ar/MLA-769464974-buzzer-activo-5v-arduino-pic-raspberry-electronica-ubot-_JM#position=2&search_layout=grid&type=item&tracking_id=f8a8f749-24b7-431c-9200-7db8ffb3a704))
- `oled_base`: Display OLED (grabar además la lib `ssd1306.py`) ([display](https://articulo.mercadolibre.com.ar/MLA-922155842-display-oled-096-blanco-128x64-i2c-arduino-ssd1306-hobb-_JM#position=11&search_layout=stack&type=item&tracking_id=fc1ddf0b-b4c5-4bd1-94bd-4d500b088449))
- `oled 1.3"`: ejemplo para display oled 0.3 (128x64), hay que usar la lib SH1106 
- `ble_rx`: Recibir por Bluetooth low energy (BLE) y mostrar en display OLED (grabar además la lib **ble_advertising.py**)
- `distance_sensor_hcsr04`: Leer distanciómetro ultrasónico analógico (grabar lib `hcsr04.py`) ([sensor](https://articulo.mercadolibre.com.ar/MLA-736251818-sensor-ultrasonido-hc-sr04-distancia-arduino-ubot-_JM#position=2&search_layout=grid&type=item&tracking_id=802680c0-2b96-4ee0-b9ba-fa7470510eac))
- `###`: Enviar por BLE lectura distanciómetro (**FALTA!**)
- `wifi_ip`: muestra la ip obtenida al conectarse a una wifi
- `data_io_en_flash`: crea directorio `datos` y archivo `test.csv` en memoria flash, escribe datos aleatorios y muestra su contenido
- `wifi_AP`: creamos un AP wifi, ojo, el ssid debe ser de 10 caracteres de largo y la pass de 8. La ip x defecto de la placa es 192.168.4.1

El ejemplo más simple es el **blink** del led onboard (Pin 2):

```python
import utime
from machine import Pin

led = Pin(2, Pin.OUT)

while True:
    led.value(not led.value())
    utime.sleep(1)
```

